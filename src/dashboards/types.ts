export interface Section {
    id: string;
    name: string;
    title: string;
    weight?: number;
    parent?: string;
    subSections: Section[];
    indicators: Dashboard[];
}

export function isSection(candidate: any) : candidate is Section {
    return !!candidate
        && typeof candidate.id === 'string'
        && typeof candidate.name === 'string'
        && typeof candidate.title === 'string'
        && (!('weight' in candidate) || typeof candidate.weight === 'number')
        && (!('parent' in candidate) || typeof candidate.parent === 'string' || candidate.parent === null)
        && (Array.isArray(candidate.subSections) || Array.isArray(candidate.indicators));
}

export interface Dashboard {
    id: string;
    adapter: string;
    remoteId: string | number;
    title: string;
    url: string;
    section: string;
}

export interface DashboardComment {
    adapter: string;
    remoteId: string;
    user: string;
    timestamp: string;
    content: string;
}
