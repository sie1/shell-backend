import { parse as parseCookie, serialize as serializeCookie } from 'cookie';
import * as FormData from 'form-data';
import { inject, injectable } from 'inversify';
import fetch, { Headers } from 'node-fetch';
import { ConfigService } from '../../config';
import { SupersetConfig } from '../../config/types';
import { INJECTABLES } from '../../injectables';
import { LimitedLifetimeValue } from '../../util/limited-lifetime-value';
import { RemoteDashboardAdapter, UNASIGNED_SECTION } from '../dashboard-service';
import { Dashboard } from '../types';

type GetFormHeaders = (userHeaders?: Record<string, string>) => Record<string, string>;

interface SuperSetDashboardDesc {
    changed_by_name: string;
    changed_on: string;
    creator: string;
    dashboard_link: string;
    dashboard_title: string;
    id: number;
    modified: string;
    url: string;
}

interface DashboardResponse {
    count: number;
    label_columns: Record<string, string>;
    list_columns: string[];
    modelview_name: 'DashboardModelViewAsync';
    order_columns: string[];
    page: number | null;
    page_size: number | null;
    pks: number[];
    result: SuperSetDashboardDesc[];
}

@injectable()
export class SupersetDashboardAdapter implements RemoteDashboardAdapter {

    readonly name = 'superset';

    private readonly supersetRootUrl: string;
    private readonly publicUrl: string;

    private readonly identity = {
        username: 'test',
        password: 'giz-meeh19'
    };

    private readonly currentSession = new LimitedLifetimeValue<Promise<string>>({ lifetime: 'P1D', initial: this.login.bind(this) });

    constructor(@inject(INJECTABLES.Config) config: ConfigService) {
        const supersetConfig = config.getSection<SupersetConfig>('superset');
        this.supersetRootUrl = supersetConfig.rootUrl;
        this.publicUrl = supersetConfig.publicUrl;
        this.identity = {
            username: supersetConfig.username,
            password: supersetConfig.password
        };
    }

    async getDashboards() : Promise<Dashboard[]> {
        const session = await this.currentSession.value;
        const headers = new Headers();
        headers.append('Cookie', serializeCookie('session', session || ''));
        const httpResponse = await fetch(
            `${this.supersetRootUrl}/dashboardasync/api/read`,
            { headers }
        );
        const superSetResponse = await httpResponse.json() as DashboardResponse;

        return superSetResponse.result.map(this.toDashboard.bind(this));
    }

    private toDashboard(assDash: SuperSetDashboardDesc) : Dashboard {
        return {
            adapter: this.name,
            remoteId: assDash.id,
            title: assDash.dashboard_title,
            url: `${this.publicUrl}/superset/dashboard/${assDash.id}?standalone=true`,
            id: '',
            section: UNASIGNED_SECTION
        };
    }

    private async login() : Promise<string> {
        const loginPageResponse = await fetch(`${this.supersetRootUrl}/login/`);
        const loginPage = await loginPageResponse.textConverted();
        const match = /<input\s[^>]*?\bname="csrf_token"\s[^>]*?\bvalue="([^"]+)"/.exec(loginPage);
        const csrf = match && match[1];
        if (!csrf) {
            throw new Error('Structure inconnue de la page de connexion de SuperSet');
        }

        const preLoginCookies = parseCookie(loginPageResponse.headers.get('Set-Cookie') || '');
        const preLoginSession = preLoginCookies.session;
        if (!preLoginSession) {
            throw new Error('Impossible de retrouver la session de SuperSet');
        }

        const form = new FormData();
        form.append('csrf_token', csrf);
        form.append('username', this.identity.username);
        form.append('password', this.identity.password);

        const loginResponse = await fetch(
            `${this.supersetRootUrl}/login/`,
            {
                method: 'POST',
                body: form,
                headers: (form.getHeaders as GetFormHeaders)({ Cookie: serializeCookie('session', preLoginSession) }),
                redirect: 'manual'
            }
        );

        if (!loginResponse.ok && ((loginResponse.status / 100) | 0) !== 3) {
            throw new Error('Impossible de se connecter à SuperSet');
        }

        if ((loginResponse.headers.get('location') || '').endsWith('/login/')) {
            throw new Error('Configuration des information d\'identification pour SuperSet invalide');
        }

        const cookies = parseCookie(loginResponse.headers.get('Set-Cookie') || '');
        const session = cookies.session;
        if (!session) {
            throw new Error('Impossible de retrouver la session de SuperSet');
        }

        return session;
    }
}
