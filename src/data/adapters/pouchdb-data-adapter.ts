import { inject, injectable } from 'inversify';
import * as moment from 'moment';
import * as PouchDB from 'pouchdb-node';
import { v1 as uuidV1 } from 'uuid';
import { ConfigService } from '../../config';
import { UNASIGNED_SECTION } from '../../dashboards/dashboard-service';
import { INJECTABLES } from '../../injectables';
import { DataAdapter, DataItem } from '../data-service';
import { StoredDashboardComment, StoredDashConfiguration, StoredSectionConfiguration } from '../types';

@injectable()
export class PouchDbDataAdapter implements DataAdapter {

    private static readonly SECTION_PREFIX = 'sec';
    private static readonly DASHBOARD_PREFIX = 'dash';

    private readonly db: PouchDB.Database;

    constructor(@inject(INJECTABLES.Config) config: ConfigService) {
        const pouchDbConfig = config.getSection<PouchDB.LevelDbAdapter.LevelDbAdapterConfiguration>('pouchDb');
        this.db = new PouchDB(pouchDbConfig.name, pouchDbConfig);
    }

    async getSections() {
        return this.getRange<StoredSectionConfiguration>(PouchDbDataAdapter.SECTION_PREFIX);
    }

    async getDashboards() {
        return this.getRange<StoredDashConfiguration>(PouchDbDataAdapter.DASHBOARD_PREFIX);
    }

    private async getRange<T>(prefix: string) : Promise<DataItem<T>[]> {
        const allDocs = await this.db.allDocs<T>(pouchIdRangeForPrefix(prefix));
        const docs = allDocs.rows.map(doc => doc.doc!);

        return docs;
    }

    async setDashboardConfig(dashboard: Partial<DataItem<StoredDashConfiguration>>) : Promise<DataItem<StoredDashConfiguration>> {
        const existing = dashboard._id
            ? await this.db
                .get<StoredDashConfiguration>(dashboard._id)
                .catch(() => undefined)
            : undefined;
        const updated = this.mergeDashEntites(dashboard, existing);
        const { id: assignedId } = await this.db.put(updated);

        return this.db.get<StoredDashConfiguration>(assignedId);
    }

    private mergeDashEntites(dashboard: Partial<DataItem<StoredDashConfiguration>>, existing: Partial<PouchDB.Core.ExistingDocument<StoredDashConfiguration>> = { }) : PouchDB.Core.PutDocument<StoredDashConfiguration> {
        const adapter = existing.adapter || dashboard.adapter;
        const remoteId = existing.remoteId || dashboard.remoteId;
        const url = existing.url || dashboard.url;
        const assignedTo = dashboard.assignedTo || existing.assignedTo || UNASIGNED_SECTION;
        const title = dashboard.title || existing.title || (remoteId || '').toString();

        if (!(adapter && remoteId && url && assignedTo && title)) {
            throw new Error('Configuration de tableau de bord incomplète');
        }

        return {
            adapter,
            remoteId,
            url,
            assignedTo,
            title,
            _id: existing._id || generateId(PouchDbDataAdapter.DASHBOARD_PREFIX),
            _rev: existing._rev
        };
    }

    async setSectionConfig(section: Partial<DataItem<StoredSectionConfiguration>>) : Promise<DataItem<StoredSectionConfiguration>> {
        const existing = section._id
            ? await this.db
                .get<StoredSectionConfiguration>(section._id)
                .catch(() => undefined)
            : undefined;
        const updated = this.mergeSectionEntities(section, existing);
        const { id: assignedId } = await this.db.put(updated);

        return this.db.get<StoredSectionConfiguration>(assignedId);
    }

    async deleteSection(id: string) : Promise<void> {
        const section = await this.db.get(id);
        await this.db.remove(id, section._rev);
    }

    private mergeSectionEntities(section: Partial<DataItem<StoredSectionConfiguration>>, existing: Partial<PouchDB.Core.ExistingDocument<StoredSectionConfiguration>> = { }) : PouchDB.Core.PutDocument<StoredSectionConfiguration> {
        const name = section.name || existing.name;
        const title = section.title || existing.title || name;

        if (!(name && title)) {
            throw new Error('Configuration incomplète de section');
        }

        return {
            name,
            title,
            parent: section.parent,
            weight: section.weight,
            _id: existing._id || generateId(PouchDbDataAdapter.SECTION_PREFIX),
            _rev: existing._rev
        };
    }

    async getComments(adapter: string, remoteId: string) : Promise<DataItem<StoredDashboardComment>[]> {
        const response = await this.db.allDocs<StoredDashboardComment>({
            startkey: `comment-${adapter}:${remoteId}`,
            endkey: `comment-${adapter}:${remoteId}-~`,
            include_docs: true
        });

        return response.rows
            .map(row => row.doc)
            .filter(Boolean) as DataItem<StoredDashboardComment>[];
    }

    async addComment(adapter: string, remoteId: string, comment: StoredDashboardComment) : Promise<DataItem<StoredDashboardComment>> {
        const ms = moment(comment.timestamp)
            .valueOf();

        const entity = {
            ...comment,
            _id: `comment-${adapter}:${remoteId}-${comment.user}-${ms}`
        };

        const response = await this.db.put(entity);
        if (!response.ok) {
            throw new Error('Impossible d\'enregistrer le commentaire');
        }

        return entity;
    }
}

function pouchIdRangeForPrefix(prefix: string) : PouchDB.Core.AllDocsWithinRangeOptions {
    return { startkey: `${prefix}-0`, endkey: `${prefix}-~`, include_docs: true };
}

function generateId(prefix: string) : string {
    const uuid = Buffer.alloc(16);
    uuidV1({ }, uuid);
    const idStr = uuid.toString('hex');

    return `${prefix}-${idStr}`;
}
