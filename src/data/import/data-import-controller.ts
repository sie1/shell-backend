import * as Router from '@koa/router';
import { mkdtemp } from 'fs';
import { BAD_REQUEST, NOT_FOUND } from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { Middleware } from 'koa';
import { promisify } from 'util';
import { Controller } from '../../app';
import { AuthService } from '../../auth';
import { INJECTABLES } from '../../injectables';
import { DataImportService } from './data-import-service';
import { NoSuchEntityError } from './no-such-file-error';

const mkdtempP = promisify(mkdtemp);

@injectable()
export class DataImportController implements Controller {

    private readonly dataAdmin: Middleware;

    constructor(
        @inject(INJECTABLES.Auth) auth: AuthService,
        @inject(INJECTABLES.DataImport) private readonly service: DataImportService
    ) {
        this.dataAdmin = auth.middleware(user => (user.groups || []).indexOf('data_admins') >= 0);
    }

    registerRoutes(router: Router, middleware?: Middleware | Middleware[], prefix: string = '') {
        const middlewares = !middleware
            ? []
            : Array.isArray(middleware)
            ? middleware
            : [middleware];

        router.get(`${prefix}/data/importer`, ...middlewares, this.dataAdmin, this.getImporters.bind(this));
        router.get(`${prefix}/data/importer/:name/example`, ...middlewares, this.getExample.bind(this));
        router.post(`${prefix}/data/importer/:name/import`, ...middlewares, this.dataAdmin, this.upload.bind(this));
        router.get(`${prefix}/data/importer/:name/import/:id`, ...middlewares, this.dataAdmin, this.getImportStatus.bind(this));
    }

    async getImporters(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.getImporters();
        } catch (e) {
            ctx.throw(e);
        }
    }

    async getExample(ctx: Router.RouterContext) {
        try {
            const example = await this.service.getExample(ctx.params.name);
            ctx.type = example.type;
            ctx.set('Content-disposition', `attachment; filename="${example.name}"`);
            ctx.body = example.stream;
        } catch (e) {
            ctx.throw(NOT_FOUND, e);
        }
    }

    async upload(ctx: Router.RouterContext) {
        if (!(ctx.request.files && ctx.request.files.file)) {
            ctx.throw(BAD_REQUEST, 'Fichier à importer est manquand');
        }

        try {
            const file = ctx.request.files!.file;
            ctx.body = await this.service.startImport(ctx.params.name, file.path, file.name);
        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }

    getImportStatus(ctx: Router.RouterContext) {
        try {
            ctx.body = this.service.getImportStatus(ctx.params.name, ctx.params.id);
        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }
}
