import { ChildProcess, spawn } from 'child_process';
import { createReadStream, mkdtemp, readdir, rename, stat, unlink } from 'fs';
import { inject, injectable } from 'inversify';
import { pickAll, template } from 'lodash/fp';
import { contentType } from 'mime-types';
import * as moment from 'moment';
import { tmpdir } from 'os';
import { join as joinPath } from 'path';
import { promisify } from 'util';
import { v4 as uuidV4 } from 'uuid';
import { ConfigService } from '../../config';
import { BaseConfig, DataImportConfig } from '../../config/types';
import { INJECTABLES } from '../../injectables';
import { DataService } from '../data-service';
import { DataExampleDto, DataImportService } from './data-import-service';
import { NoSuchEntityError } from './no-such-file-error';
import { DataImporter, DataImporterDto, ImportStatus, isDataImporter } from './types';

const mkdtempP = promisify(mkdtemp);
const readdirP = promisify(readdir);
const renameP = promisify(rename);
const statP = promisify(stat);

interface ImportProcess {
    id: string;
    type: string;
    start: moment.Moment;
    end?: moment.Moment;
    status: 'running' | 'done' | 'failed';
    process: ChildProcess | null;
    file: string;
    exitCode?: number;
    sqlOutput: boolean;
    output?: string;
}

@injectable()
export class DataImportServiceImpl implements DataImportService {

    private readonly baseConfig: BaseConfig;
    private readonly config: DataImportConfig;
    private readonly importProcessRegistry: Map<string, ImportProcess> = new Map();

    constructor(
        @inject(INJECTABLES.Config) configService: ConfigService,
        @inject(INJECTABLES.Data) private readonly dataService: DataService
    ) {
        this.baseConfig = configService.getSection<BaseConfig>('base');
        this.config = configService.getSection<DataImportConfig>('dataImport');
    }

    async getImporters() {
        const apiHost = this.baseConfig.apiHostName
            || (this.baseConfig.rootHostName
                ? `api.${this.baseConfig.rootHostName}`
                : 'localhost'
            );
        const protocol = this.baseConfig.rootProtocol || 'http';

        if (!(await statP(this.config.importersDir)).isDirectory()) {
            console.warn('Importers dicrectory missing');

            return [];
        }

        const importerDirs = (await readdirP(this.config.importersDir, { withFileTypes: true }))
            .filter(dirEnt => dirEnt.isDirectory());

        const importers = await Promise.all(
            importerDirs.map(
                dirEnt => this
                    .loadDataImporter(dirEnt.name)
                    .catch(() => null)
            )
        );

        return (importers
            .filter(Boolean) as DataImporter[])
            .map(importer => {
                const mapped: DataImporterDto = {
                    name: importer.name,
                    label: importer.label,
                    description: importer.description
                };

                if (importer.fileTypes) {
                    mapped.fileTypes = importer.fileTypes;
                }

                if (importer.example) {
                    mapped.example = `${protocol}://${apiHost}/data/importer/${importer.name}/example`;
                }

                return mapped;
            });
    }

    async getExample(importer: string) : Promise<DataExampleDto> {
        if (/[/\\]|\.\./.test(importer)) {
            throw new Error('Nom d\'importeur illégal');
        }

        try {
            const importerDefinition = await this.loadDataImporter(importer);
            if (!importerDefinition.example) {
                throw new Error();
            }

            return {
                type: contentType(importerDefinition.example) || 'application/octet-stream',
                name: importerDefinition.example,
                stream: createReadStream(joinPath(this.config.importersDir, importer, importerDefinition.example), { autoClose: true })
            };
        } catch (e) {
            throw new Error('Impossible de retrouver l\'example');
        }
    }

    async startImport(type: string, filePath: string, fileOrigName: string) : Promise<ImportStatus> {
        const importer = await this.loadDataImporter(type);
        const id = uuidV4();

        const tempDir = await mkdtempP(joinPath(tmpdir(), 'meeh-import'));
        const correctedFilePath = joinPath(tempDir, fileOrigName);
        await renameP(filePath, correctedFilePath);

        const commandTemplate = template(importer.command);
        const command = commandTemplate({
            file: correctedFilePath,
            ...pickAll([ 'dbHost', 'dbPort', 'dbName', 'dbUser', 'dbPass' ], this.config)
        });

        const importerDir = this.getImporterDir(type);
        const process = spawn(command, { shell: true, cwd: importerDir, stdio: 'pipe' });
        const start = moment();

        const description: ImportProcess = {
            id,
            type,
            start,
            process,
            file: correctedFilePath,
            sqlOutput: importer.output === 'sql',
            status: 'running'
        };

        this.importProcessRegistry.set(description.id, description);
        this.trackImportProcess(description.id);

        return this.getStatusForImportProcess(description);
    }

    getImportStatus(type: string, id: string) : ImportStatus {
        const process = this.importProcessRegistry.get(id);
        if (!process || process.type !== type) {
            throw new NoSuchEntityError(`${type}/${id}`);
        }

        return this.getStatusForImportProcess(process);
    }

    private trackImportProcess(id: string) {
        const processDesc = this.importProcessRegistry.get(id)!;
        const process = processDesc.process!;
        const outputChunks: Buffer[] = [];
        process.stdout!.on(
            'data',
            chunk => outputChunks.push(
                Buffer.isBuffer(chunk)
                    ? chunk
                    : typeof chunk === 'string'
                    ? Buffer.from(chunk, 'utf8')
                    : Buffer.from(chunk)
            )
        );

        function cleanup() {
            outputChunks.splice(0);
            process.unref();
            unlink(processDesc.file, () => undefined);
        }

        process.on('close', (code, signal) => {
            processDesc.end = moment();
            processDesc.status = code === 0 ? 'done' : 'failed';
            processDesc.exitCode = code;
            processDesc.output = Buffer.concat(outputChunks)
                .toString('utf8');
            if (signal) {
                console.error(`Import process was killed by signal ${signal}`);
            }

            if (!code && !signal && processDesc.sqlOutput) {
                processDesc.status = 'running';
                processDesc.end = undefined;
                this.executeProcessSql(processDesc);
            } else {
                cleanup();
            }
        });

        process.on('error', err => {
            console.error(err);
            processDesc.end = moment();
            processDesc.status = 'failed';
            processDesc.exitCode = Number.MIN_SAFE_INTEGER;

            cleanup();
        });
    }

    private getStatusForImportProcess(importProcess: ImportProcess) : ImportStatus {
        const status: ImportStatus = {
            importId: importProcess.id,
            start: importProcess.start.format(),
            status: importProcess.status
        };

        if (importProcess.end) {
            status.end = importProcess.end.format();
            status.output = importProcess.output;
        }

        return status;
    }

    private async loadDataImporter(name: string) : Promise<DataImporter> {
        const filename = joinPath(this.getImporterDir(name), 'manifest.json');
        if (!(await statP(filename)).isFile()) {
            throw new NoSuchEntityError(filename);
        }

        const contents = require(filename);
        if (!isDataImporter(contents)) {
            throw new Error(`Manifest invalide pour l'importeur ${name}`);
        }

        return contents;
    }

    private getImporterDir(name: string) {
        return joinPath(this.config.importersDir, name);
    }

    private executeProcessSql(proc: ImportProcess) {
        this.dataService.warehouse
            .transaction()
            .then(async trx => {
                try {
                    proc.output = await trx.raw(proc.output || '');
                    trx.commit();
                    proc.status = 'done';
                } catch (e) {
                    trx.rollback(e);
                    proc.status = 'failed';
                    proc.output = e.message || e;
                }

                proc.end = moment();
            })
            .catch(e => console.error(e));
    }
}
