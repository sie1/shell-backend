export class NoSuchEntityError extends Error {
    constructor(readonly entityname: string, msg?: string) {
        super(msg);
    }
}
