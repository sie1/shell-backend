import { Readable } from 'stream';
import { DataImporterDto, ImportStatus } from './types';

export interface DataExampleDto {
    type: string;
    name: string;
    stream: Readable;
}

export interface DataImportService {
    getImporters() : Promise<DataImporterDto[]>;
    getExample(importer: string) : Promise<DataExampleDto>;

    startImport(type: string, filePath: string, fileOrigName: string) : Promise<ImportStatus>;
    getImportStatus(type: string, id: string) : ImportStatus;
}
