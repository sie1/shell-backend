export interface DataImporterDto {
    name: string;
    label: string;
    description: string;
    example?: string;
    fileTypes?: string;
}

export interface DataImporter extends DataImporterDto {
    command: string;
    output: 'sql' | 'none';
}

export function isDataImporter(candidate: any) : candidate is DataImporter {
    return typeof candidate === 'object'
        && !!candidate
        && typeof candidate.name === 'string'
        && typeof candidate.label === 'string'
        && typeof candidate.description === 'string'
        && typeof candidate.command === 'string'
        && typeof candidate.output === 'string';
}

export interface ImportStatus {
    importId: string;
    status: 'running' | 'done' | 'failed';
    start: string;
    end?: string;
    output?: string;
  }
