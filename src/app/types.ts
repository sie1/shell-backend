import * as Router from '@koa/router';
import { ParameterizedContext } from 'koa';
import { Middleware } from 'koa-compose';

export interface RouterService {
    readonly routes: Middleware<ParameterizedContext<any, Router.RouterContext>>;
}

export interface App {
    start(routes: Middleware<ParameterizedContext<any, Router.RouterContext>>) : void;
}

export interface Controller {
    registerRoutes(router: Router) : void;
}
