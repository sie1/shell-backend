import { App, RouterService } from './app';
import { INJECTABLES } from './injectables';
import { container } from './inversify.config';

const app = container.get<App>(INJECTABLES.App);
const router = container.get<RouterService>(INJECTABLES.Router);

app.start(router.routes);
