import { DataImportConfig } from './types';

export const dataImportConfig: DataImportConfig = {
    importersDir: '{{data-dir}}/importers'
};
