export const pouchDbConfig: PouchDB.LevelDbAdapter.LevelDbAdapterConfiguration = {
    adapter: 'leveldb',
    name: 'ldb://{{data-dir}}/data.ldb/'
};
