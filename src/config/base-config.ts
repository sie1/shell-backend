import * as yargs from 'yargs';
import { baseConfigDefaults } from './base-config-defaults';

export const baseConfig = yargs
    .option('t', {
        describe: 'maximum time to wait for LDAP response in ms',
        type: 'number'
    })
    .alias('t', 'ldaptimeout')
    .option('p', {
        describe: 'set the port the server is listening on',
        type: 'number'
    })
    .alias('p', 'port')
    .option('h', { type: 'string' })
    .alias('h', 'rootHostName')
    .option('root-protocol', { type: 'string' })
    .option('l', { type: 'string' })
    .alias('l', 'ldap-host')
    .option('ldap-base-dn', { type: 'string' })
    .option('ldap-admin-password', { type: 'string' })
    .option('carto-host', { type: 'string' })
    .option('carto-user', { type: 'string' })
    .option('carto-pass', { type: 'string' })
    .option('superset-host', { type: 'string' })
    .option('superset-public-host', { type: 'string' })
    .option('superset-user', { type: 'string' })
    .option('superset-pass', { type: 'string' })
    .option('d', { type: 'string' })
    .alias('d', 'data-dir')
    .option('api-host-name', { type: 'string' })
    .option('db-host', { type: 'string' })
    .option('db-port', { type: 'number' })
    .option('db-name', { type: 'string' })
    .option('db-user', { type: 'string' })
    .option('db-pass', { type: 'string' })
    .option('mariadb-host', { type: 'string' })
    .default(baseConfigDefaults)
    .argv;
