export interface BaseConfig {
    port: number;
    ldapTimeout: number;
    dataDir: string;
    rootHostName: string;
    rootProtocol: string;
    apiHostName: string;
    ldapHost: string;
    ldapBaseDn: string;
    ldapAdminPassword: string;
    cartoHost: string;
    cartoUser: string;
    cartoPass: string;
    supersetHost: string;
    supersetPublicHost: string;
    supersetUser: string;
    supersetPass: string;
    dbHost: string;
    dbPort: number;
    dbName: string;
    dbUser: string;
    dbPass: string;
    mariadbHost: string;
}

export interface LdapConfig {
    url: string;
    port: number;
    bind: string;
    bindPw: string;
    baseDn: string;
    usersUnit: string;
    groupsUnit: string;
    id: string;
    firstName: string;
    lastName: string;
    mail: string;
    timeout: string;
}

export interface CartoDbConfig {
    rootUrl: string;
    username: string;
    password: string;
}

export interface SupersetConfig {
    rootUrl: string;
    publicUrl: string;
    username: string;
    password: string;
}

export interface DataImportConfig {
    importersDir: string;
}

export interface DatabaseConfig {
    postgresHost: string;
    postgresPort: number;
    postgresDb: string;
    postgresUser: string;
    postgresPassword: string;
    mariadbHost: string;
}
