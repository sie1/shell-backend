import { CartoDbConfig } from './types';

export const cartodbConfig: CartoDbConfig = {
    rootUrl: '{{carto-host}}',
    username: '{{carto-user}}',
    password: '{{carto-pass}}'
};
