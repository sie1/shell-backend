import * as Router from '@koa/router';
import { inject, injectable } from 'inversify';
import { Middleware } from 'koa';
import { Controller } from '../../app';
import { AuthService } from '../../auth';
import { DataImportService } from '../../data';
import { INJECTABLES } from '../../injectables';
import { JoomlaFixerService } from './joomla-fixer-service';

@injectable()
export class JoomlaFixerController implements Controller {

    private readonly dataAdmin: Middleware;

    constructor(
        @inject(INJECTABLES.Auth) auth: AuthService,
        @inject(INJECTABLES.DataImport) private readonly service: DataImportService,
        @inject(INJECTABLES.JoomlaFixer) private readonly joomlaFixer: JoomlaFixerService
    ) {
        this.dataAdmin = auth.middleware(user => (user.groups || []).indexOf('data_admins') >= 0);
    }

    registerRoutes(router: Router, middleware?: Middleware | Middleware[], prefix: string = '') {
        const middlewares = !middleware
            ? []
            : Array.isArray(middleware)
            ? middleware
            : [middleware];

        router.get(`${prefix}/tasks/joomla/prefixes`, ...middlewares, this.dataAdmin, this.getPrefixes.bind(this));
        router.get(`${prefix}/tasks/joomla/prefixes/:prefix/domains`, ...middlewares, this.dataAdmin, this.getConfiguredDomains.bind(this));
        router.post(`${prefix}/tasks/joomla/prefixes/:prefix/domains`, ...middlewares, this.dataAdmin, this.changeConfiguredDomains.bind(this));
    }

    async getPrefixes(ctx: Router.RouterContext) {
        ctx.body = {
            prefixes: await this.joomlaFixer.getPrefixes()
        };
    }

    async getConfiguredDomains(ctx: Router.RouterContext) {
        try {
            ctx.body = {
                domains: await this.joomlaFixer.getConfiguredDomains(ctx.params.prefix || '')
            };
        } catch (e) {
            this.handleErrorForContext(ctx, e);
        }
    }

    async changeConfiguredDomains(ctx: Router.RouterContext) {
        const prefix = ctx.params.prefix || '';
        const from = ctx.request.body.from || 'sie.mg';
        const to = ctx.request.body.to || 'sie.mg';

        try {
            ctx.body = await this.joomlaFixer.changeDomain(prefix, from, to);
        } catch (e) {
            this.handleErrorForContext(ctx, e);
        }
    }

    private handleErrorForContext(ctx: Router.RouterContext, e: any) : never {
        if ((e instanceof Error) && ('code' in e)) {
            if ((e as { code: string }).code === 'ER_NO_SUCH_TABLE') {
                ctx.throw(404, e);
            }
        }

        return ctx.throw(500, e);
    }
}
