export interface JoomlaFixerService {
    getPrefixes() : Promise<string[]>;
    getConfiguredDomains(prefix: string) : Promise<string[]>;
    changeDomain(prefix: string, from: string, to: string) : Promise<number>;
}
