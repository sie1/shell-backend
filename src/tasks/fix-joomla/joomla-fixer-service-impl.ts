import { inject, injectable } from 'inversify';
import * as Knex from 'knex';
import { uniq } from 'lodash/fp';
import { ConfigService } from '../../config';
import { DatabaseConfig } from '../../config/types';
import { INJECTABLES } from '../../injectables';
import { LimitedLifetimeValue } from '../../util/limited-lifetime-value';
import { JoomlaFixerService } from './joomla-fixer-service';

@injectable()
export class JoomlaFixerServiceImpl implements JoomlaFixerService {

    private readonly config: DatabaseConfig;
    private readonly knex: LimitedLifetimeValue<Knex>;

    constructor(
        @inject(INJECTABLES.Config) configService: ConfigService
    ) {
        this.config = configService.getSection<DatabaseConfig>('database');
        this.knex = new LimitedLifetimeValue({
            lifetime: 'PT15M',
            initial: () => this.initKnex(),
            autoTouch: true,
            unref: true,
            destructor: (knex: Knex) => knex
                .destroy()
                .then()
        });
    }

    private initKnex() : Knex {
        return Knex({
            dialect: 'mysql',
            client: 'mysql',
            connection: {
                host: this.config.mariadbHost,
                user: 'root',
                password: this.config.postgresPassword,
                database: 'joomla'
            }
        });
    }

    async getPrefixes() : Promise<string[]> {
        const knex = this.knex.value!;

        const result = await knex.raw('show tables');
        const allPrefixes = (result[0] as { Tables_in_joomla: string }[])
            .map(table => /^(.+?_)/.exec(table.Tables_in_joomla))
            .filter(Boolean)
            .map(match => match![0]);

        return uniq(allPrefixes);
    }

    async getConfiguredDomains(prefix: string) : Promise<string[]> {
        const knex = this.knex.value!;

        const result = await knex(`${cleanPrefix(prefix)}modules`)
            .where('module', 'mod_gantry5_particle')
            .select('params');

        const allDomains = (result as { params: string }[])
            .map(row => {
                const params = JSON.parse(row.params);
                const particle = JSON.parse(params.particle);

                return particle.options.particle.html;
            })
            .map(html => /<iframe\s[^>]*\bsrc="([^"]*)"/.exec(html))
            .filter(Boolean)
            .map(url => new URL(url![1]).host)
            .map(host => /^(?:carto(?:db)?\.|superset\.)?(.*)/.exec(host)![1]);

        return uniq(allDomains);
    }

    async changeDomain(prefix: string, from: string | string[], to: string) : Promise<number> {
        const knex = this.knex.value!;
        const domains = (Array.isArray(from) ? from : [from]).join('|');

        return knex(`${cleanPrefix(prefix)}modules`)
            .update({
                params: knex.raw(
                    `REGEXP_REPLACE(
                        ??,
                        '(src=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\")\\\\s*(http|https)(:[^"]*)(${domains})([^"]*\\\\s*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\")',
                        '\\\\1http\\\\3${to}\\\\5'
                    )`,
                    ['params']
                )
            })
            .where('module', 'mod_gantry5_particle')
            .andWhereRaw(`?? REGEXP 'src=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"[^"]*(${domains})[^"]*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"'`, ['params']);
    }

}

function cleanPrefix(prefix: string) : string {
    return !prefix || prefix === '_' ? '' : prefix;
}
